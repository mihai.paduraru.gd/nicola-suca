<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function showHomepage() {
        return view('home');
    }

    public function showChisiamo() {
        return view('chisiamo');
    }

    public function showServizi() {
        return view('servizi');
    }

    public function showContatti() {
        return view('contatti');
    }
}
