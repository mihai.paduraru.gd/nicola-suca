<x-layout>

    <header id="header">
        <div class="img-top"></div>
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <h1 class="head-title">SORRISO BRILLANTE</h1>
                    <p class="content-head-top">
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. <br>
                        Eos hic optio praesentium aspernatur harum corrupti veniam <br>
                        perspiciatis a ipsum facilis beatae nisi neque, dignissimos <br>
                        consequuntur molestiae. Porro veritatis sit quasi.
                    </p>
                    <p class="content-head-bottom">
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. <br>
                        Eos hic optio praesentium aspernatur harum corrupti veniam <br>
                        perspiciatis a ipsum facilis beatae nisi neque, dignissimos <br>
                        consequuntur molestiae. Porro veritatis sit quasi.
                    </p>
                </div>
            </div>
        </div>

    </header>

    <main id="main">
        <div class="img-center"></div>
    </main>

    <section id="section-1">

    </section>

    <section id="section-2">

    </section>

    <footer id="footer">

    </footer>

</x-layout>