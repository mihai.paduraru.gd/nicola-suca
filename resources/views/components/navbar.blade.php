<nav id="nav">
    <ul id="navbar-list">
        <li class="list-nav">
            <a class="link-list" href="{{route('home')}}">Homepage</a>
        </li>
        <li class="list-nav">
            <a class="link-list" href="{{route('description')}}">Chi siamo</a>
        </li>
        <li class="list-nav">
            <a class="link-list" href="{{route('service')}}">Servizi</a>
        </li>
        <li class="list-nav">
            <a class="link-list" href="{{route('contact')}}">Contatti</a>
        </li>
    </ul>
</nav>