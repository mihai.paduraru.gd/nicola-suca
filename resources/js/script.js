const navbar = document.querySelector('#nav');

window.addEventListener('scroll', () => {
    let scrolled = window.scrollY;

    if (scrolled > 0) {
        navbar.style.backgroundColor = 'rgba(255, 255, 255, 0.3)';
    } else {
        navbar.style.backgroundColor = 'rgba(0, 0, 0, 0)';
    }
});


const imgTop = document.querySelector('.img-top');
const main = document.querySelector('#main');

let observerT = new IntersectionObserver(
    (entries) => {
        entries.forEach(entry => {
            
            if (entry.isIntersecting) {
                imgTop.style.transform = 'translateX(61vw)';
                imgTop.style.opacity = '1';
            } else {
                imgTop.style.transform = 'translateX(71vw)';
                imgTop.style.opacity = '0';
            }
        })
    }
);

observerT.observe(main);


const imgCenter = document.querySelector('.img-center');
const section1 = document.querySelector('#section-1')

let observerM = new IntersectionObserver(
    (entries) => {
        entries.forEach(entry => {
            
            if (entry.isIntersecting) {
                imgCenter.style.transform = 'translateX(5vw)';
                imgCenter.style.opacity = '1';

            } else {
                imgCenter.style.transform = 'translateX(-35vw)';
                imgCenter.style.opacity = '0';
            }
        })
    }
);

observerM.observe(section1);


const title = document.querySelector('.head-title');
const contentTop = document.querySelector('.content-head-top');
const contentBottom = document.querySelector('.content-head-bottom');

let observer = new IntersectionObserver(
    (entries) => {
        entries.forEach(entry => {
            
            if (entry.isIntersecting) {
                title.style.transform = 'translateX(0vw)';
                title.style.opacity = '1';

                contentTop.style.transform = 'translateX(0vw)';
                contentTop.style.opacity = '1';

                contentBottom.style.transform = 'translateX(0vw)';
                contentBottom.style.opacity = '1';
            } 
            
            else {
                title.style.transform = 'translateX(5vw)';
                title.style.opacity = '0';
                
                contentTop.style.transform = 'translateX(5vw)';
                contentTop.style.opacity = '0';

                contentBottom.style.transform = 'translateX(5vw)';
                contentBottom.style.opacity = '0';
            }
        })
    }
);

observer.observe(main);